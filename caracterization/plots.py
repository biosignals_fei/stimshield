import pandas as pd
import matplotlib
import matplotlib.pyplot as plt


# default settings
matplotlib.rcParams.update({'font.size': 24})
fig_size = 12, 6

# dados 1
df_pulso = pd.read_csv("./data/NewFile2.csv", skiprows=1)
df_pulso.Second = df_pulso.Second*1e6
df_pulso.set_index('Second', inplace=True)
df_pulso.drop('Unnamed: 2', axis=1, inplace=True)
# plot 1
ax = df_pulso.plot(title="Pulso Bifásico", legend=False, figsize=(fig_size))
ax.set_xlabel(r'Tempo [$\mu s$]')  # raw string TeX text
ax.set_ylabel(r'Amplitude [$V$]')  # raw string TeX text
plt.grid()
plt.savefig('./pulso_osciloscope.png', bbox_inches='tight')
plt.savefig('./pulso_osciloscope.svg', bbox_inches='tight')
plt.savefig('./pulso_osciloscope.eps', bbox_inches='tight')
plt.close()

# dados 2
df_rampa = pd.read_csv("./data/Rampa 2.csv", skiprows=1)
df_rampa.Second = df_rampa.Second*1e3
df_rampa.set_index('Second', inplace=True)
df_rampa.drop('Unnamed: 2', axis=1, inplace=True)
# plot 2
ax = df_rampa.plot(title="Rampa de Subida", legend=False, figsize=(fig_size))
ax.set_xlabel(r'Tempo [$m s$]')  # raw string TeX text
ax.set_ylabel(r'Amplitude [$V$]')  # raw string TeX text
plt.grid()
plt.savefig('./rampa_osciloscope.png',bbox_inches='tight')
plt.savefig('./rampa_osciloscope.svg',bbox_inches='tight')
plt.savefig('./rampa_osciloscope.eps',bbox_inches='tight')
plt.close()
