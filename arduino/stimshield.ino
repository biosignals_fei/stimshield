//  _ ___ _______     ___ ___ ___  ___ _   _ ___ _____ ___ 
// / |_  )__ /   \   / __|_ _| _ \/ __| | | |_ _|_   _/ __| 
// | |/ / |_ \ |) | | (__ | ||   / (__| |_| || |  | | \__ \ 
// |_/___|___/___/   \___|___|_|_\\___|\___/|___| |_| |___/ 
// 
// Stim Shield
// 
// Made by Rodrjgo Piva
// License: CC-BY-SA 3.0
// Downloaded from: https://123d.circuits.io/circuits/1028691-stim-shield

/* Blink without Delay

 Este programa realiza a ativação dos dois canais do shield de eletroestimulador
 Stim shield. as saidas da placa do arduino estao configuradas abaixo para gerar
 um pulso de 300us em sequencia.
 O circuito:
 * Pino 28 fase positiva do canal 1
 * Pino 04 fase negativa do canal 1
 * Pino 12 fase positiva do canal 2
 * Pino 27 fase negativa do canal 2
 * Pino 17 pwm1
 * Pino 03 pwm2
 * 
 * Nota: esta programação deve ser utilizada com cautela e somente com 
 * a placa stim shield em sua configuração original.
 * 
 criado em 2016
 by Rodrigo G. G. Piva
 by Renato Gomes Barelli
 by Rodrigo Pior Bechelli
 
 
 This example code is in the public domain.
 

 */
/*
// constants won't change. Used here to set a pin number :
const int ledPin =  13;      // the number of the LED pin
const int Canal1P = PC5;      // Canal 1 fase positiva
const int Canal1N = PD2;      // Canal 1 fase negativa
const int Canal2P = PB4;      // Canal 2 fase positiva
const int Canal2N = SDA;      // Canal 2 fase negativa
const int PWMm1   = PB3;      // Canal 2 fase negativa
const int PWMm2   = PD3;      // Canal 2 fase negativa*/


// constants won't change. Used here to set a pin number :
const int ledPin =  13;      // the number of the LED pin
const int Canal1P = SCL;      // Canal 1 fase positiva
const int Canal1N = PD2;      // Canal 1 fase negativa
const int Canal2P = MISO;      // Canal 2 fase positiva
const int Canal2N = SDA;      // Canal 2 fase negativa
const int PWMm1   = PB3;      // Canal 2 fase negativa
const int PWMm2   = PD3;      // Canal 2 fase negativa


// Variables will change :
int ledState = LOW;             // ledState used to set the LED
int Canal1Pstate = LOW;     // estado inicial do canal 1
int Canal1Nstate = LOW;     // estado inicial do canal 1
int Canal2Pstate = LOW;     // estado inicial do canal 2
int Canal2Nstate = LOW;     // estado inicial do canal 2
int PWMm1state   = LOW;     // estado inicial do pwmm1
int PWMm2state   = LOW;     // estado inicial do pwmm2


void setup() 
{
  //configuração das entradas
  pinMode(ledPin, OUTPUT);
  pinMode(Canal1P,OUTPUT);
  pinMode(Canal1N,OUTPUT);
  pinMode(Canal2P,OUTPUT);
  pinMode(Canal2N,OUTPUT);
  pinMode(PWMm1,  INPUT);
  pinMode(PWMm2,  INPUT);
}

void loop()
{
  digitalWrite(ledPin, HIGH);
  
  digitalWrite(Canal1P, HIGH);   // liga o canal1P
  
  delayMicroseconds(150);        // pausa por 300 microsegundos
  delayMicroseconds(150);        // pausa por 300 microsegundos
        
  digitalWrite(Canal1P, LOW);    // desliga o canal1P
  
  digitalWrite(Canal1N, HIGH);   // liga o canal1N

  delayMicroseconds(150);        // pausa por 300 microsegundos
  delayMicroseconds(150);        // pausa por 300 microsegundos
  
  digitalWrite(Canal1N, LOW);   // desliga o canal1N
          
 // delayMicroseconds(12200);
 // delayMicroseconds(12200);
  delayMicroseconds(12200);
  delayMicroseconds(12200);

  digitalWrite(Canal2P, HIGH);   // liga o canal2P
  
  delayMicroseconds(150);        // pausa por 300 microsegundos
  delayMicroseconds(150);        // pausa por 300 microsegundos
        
  digitalWrite(Canal2P, LOW);    // desliga o canal2P
  
  digitalWrite(Canal2N, HIGH);   // liga o canal2N

  delayMicroseconds(150);        // pausa por 300 microsegundos
  delayMicroseconds(150);        // pausa por 300 microsegundos
  
  digitalWrite(Canal2N, LOW);   // desliga o canal2N
      
  delayMicroseconds(12200);
  delayMicroseconds(12200);
  
  digitalWrite(ledPin, LOW);
  
}
