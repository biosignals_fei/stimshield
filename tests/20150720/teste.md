# Teste de Circuíto

Data: 20/07/2016
Testador: Renato Barelli

## Equipamentos utilizados

* Stimshield: versão 0.01
* Osciloscópio: Agilent DSO-X3012A (calibrado)

Ver './registro_do_ensaio'

## Osciloscópio

* Calibrado e certificado
* Incerteza tipo B1 (resolução do osciloscópio): 0,00057735V
* Incerteza tipo B2 (incerteza do osciloscópio): Teremos que analisar o certificado de calibração e extrair este valor de alguma forma

## Simluação e resultados

Foram medidos 3 csv de 3 telas, todas com 10 ciclos, e 16000 pontos

Os resultados da simulação estão em './amplitude_e_frequencia'
