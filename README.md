# StimShield - Project Arduino Shield Stimulator

Electrical (Neuro)muscular Stimulation is a research field that attracts educational and rehabilitation initiatives.

*The proposed circuitry provides two constant current independent stimulation channels with up to eight channels cascade integration. The output signal consist of biphasic pulses, of 300 uS width and 20 Hz frequency.*


# Citation

In order to support this project, please use this reference:

Barelli, R. G., Piva, R. G. G., Bechelli, R. P., & Castro, M. C. F. (2016). Stimshield – shield para arduíno uno® com dois canais de estimulação elétrica neuromuscular. In XXV Congresso Brasileiro de Engenharia Biomédica (CBEB 2016) (pp. 412–415). Foz do Iguaçu. ISSN:2359-3164

## Latex

        @inproceedings{Barelli2016,
        abstract = {A growing interest among students for the free and worldwide known Arduino, motivated the development of this shield configurable integrated circuit. Electrical Neuromuscular Stimulation is a research field that attracts educational and rehabilitation initiatives. The proposed circuitry provides two constant current independent stimulation channels with up to eight channels cascade integration. The output signal consist of biphasic pulses, of 300 uS width and 20 Hz frequency.},
        address = {Foz do Igua{\c'{c}}u},
        author = {Barelli, Renato G and Piva, Rodrigo G G and Bechelli, R. P. and Castro, Maria Claudia F.},
        booktitle = {XXV Congresso Brasileiro de Engenharia Biom{\'{e}}dica (CBEB 2016)},
        doi = {ISSN:2359-3164},
        keywords = {Ardu{\'{i}}no,Estimula{\c{c}}{\~{a}}o El{\'{e}}trica Neuromuscular,Shield},
        pages = {412--415},
        title = {{Stimshield – shield para ardu{\'{i}}no uno{\textregistered} com dois canais de estimula{\c{c}}{\~{a}}o el{\'{e}}trica neuromuscular}},
        url = {https://drive.google.com/drive/folders/0B543adcG1FClQ21ZaFhCUmdwMlk?usp=sharing},
        year = {2016}
        }
